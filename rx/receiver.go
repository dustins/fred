package rx

import (
	"code.swigg.net/dustins/fred/utils"
	"github.com/klauspost/reedsolomon"
	"log"
	"net"
	"time"
)

func Receiver(config utils.FREDConfig) {
	conn := createConnection(config)
	// 	enc := createEncoder(config)

	bytesRead := 0
	var start time.Time

	fileBytes, returnBytes := readBytes(config)
	go func(fileBytes chan []byte, returnBytes chan []byte) {
		for {
			buf := <-returnBytes
			n, err := conn.Read(buf)
			utils.CheckError(err, true)
			if n == 1 {
				break
			}
			fileBytes <- buf
		}
		close(fileBytes)
	}(fileBytes, returnBytes)

	for buffer := range fileBytes {
		if bytesRead == 0 {
			start = time.Now()
		}
		bytesRead += len(buffer)
		returnBytes <- buffer
	}
	duration := time.Since(start)
	close(returnBytes)
	log.Printf("Read %s bytes in %.2fs at %s/s",
		utils.ByteCountIEC(int64(bytesRead)),
		duration.Seconds(),
		utils.ByteCountIEC(int64(float64(bytesRead)/duration.Seconds())))
}

func readBytes(config utils.FREDConfig) (fileBytes chan []byte, returnBytes chan []byte) {
	fileBytes = make(chan []byte, config.ChannelDepth)
	returnBytes = make(chan []byte, cap(fileBytes))
	for x := 0; x < cap(fileBytes); x++ {
		returnBytes <- make([]byte, config.PacketSize)
	}

	return fileBytes, returnBytes
}

func createEncoder(config utils.FREDConfig) reedsolomon.Encoder {
	enc, err := reedsolomon.New(config.DataShards, config.ParityShards)
	utils.CheckError(err, true)

	return enc
}

func createConnection(config utils.FREDConfig) net.Conn {
	raddr, err := net.ResolveUDPAddr("udp4", config.Raddr)
	utils.CheckError(err, true)
	conn, err := net.ListenUDP("udp4", raddr)
	utils.CheckError(err, true)

	return conn
}
