package main

import (
	"code.swigg.net/dustins/fred/rx"
	"code.swigg.net/dustins/fred/tx"
	"code.swigg.net/dustins/fred/utils"
	"flag"
	"fmt"
)

func main() {
	raddr := flag.String("raddr", "127.255.255.255:4444", "remote ipv4 address:port")
	packetSize := flag.Int("packetSize", 8950, "size of packets")
	dataShards := flag.Int("dataShards", 17, "number of data shards")
	parityShards := flag.Int("parityShards", 3, "number of parity shards")
	benchmark := flag.Bool("benchmark", false, "perform benchmark")
	size := flag.Int("size", 1024*1024*1000, "size to benchmark")
	fbufferSize := flag.Int("fbufferSize", 1, "size of file read buffer")
	channelDepth := flag.Int("channelDepth", 1, "size of channels")
	delay := flag.Int("delay", 0, "nanoseconds delay between each packet")
	flag.Parse()

	if len(flag.Args()) > 0 {
		config := utils.FREDConfig{flag.Args()[1:], *raddr, *packetSize, *dataShards, *parityShards, *benchmark, *size, *fbufferSize, *channelDepth, *delay}

		switch flag.Args()[0] {
		case "tx":
			tx.Sender(config)
		case "rx":
			rx.Receiver(config)
		default:
			flag.Usage()
		}
	} else {
		flag.Usage()
	}

	fmt.Println("Done.")
}
