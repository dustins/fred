module code.swigg.net/dustins/fred

go 1.13

require (
	github.com/klauspost/cpuid v1.2.3 // indirect
	github.com/klauspost/reedsolomon v1.9.3
)
