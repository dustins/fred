package tx

type FileBytes struct {
	Size  int
	Bytes []byte
}
