package tx

import (
	"bufio"
	"code.swigg.net/dustins/fred/utils"
	"github.com/klauspost/reedsolomon"
	"log"
	"net"
	"os"
	"time"
)

type Chunk struct {
	fileBytes *FileBytes
	shards    *[][]byte
}

func Sender(config utils.FREDConfig) {
	conn := createConnection(config)
	enc := createEncoder(config)

	// delay := time.Duration(config.Delay)
	bytesRead := 0
	bytesWrote := 0
	start := time.Now()
	readBytes, returnBytes := loadBytes(config)
	chunkId := byte(0)

	chunks := make(chan Chunk, config.ChannelDepth)
	chunkReturn := make(chan Chunk, cap(chunks))
	for i := 0; i < cap(chunkReturn); i++ {
		chunkReturn <- Chunk{}
	}
	go writeBytes(chunks, chunkReturn, returnBytes, conn)

	for fileBytes := range readBytes {
		bytesRead += fileBytes.Size

		shards, err := enc.Split(fileBytes.Bytes)
		utils.CheckError(err, true)

		err = enc.Encode(sliced(shards))
		utils.CheckError(err, true)

		chunk := <-chunkReturn
		chunk.fileBytes = &fileBytes
		chunk.shards = &shards
		chunks <- chunk

		for _, shard := range shards {
			bytesWrote += len(shard)
		}

		chunkId++
	}
	close(chunks)
	func() {
		for {
			select {
			case <-chunkReturn:
				return
			default:
			}
		}
	}()
	duration := time.Since(start)

	log.Printf("Sent %s file in %.2fs at %s/s",
		utils.ByteCountIEC(int64(bytesRead)),
		duration.Seconds(),
		utils.ByteCountIEC(int64(float64(bytesRead)/duration.Seconds())))

	log.Printf("Wrote %s (%.2f%% overhead with %dd/%dp) at %s/s",
		utils.ByteCountIEC(int64(bytesWrote)),
		float64(bytesWrote)/float64(bytesRead)*100-100,
		config.DataShards,
		config.ParityShards,
		utils.ByteCountIEC(int64(float64(bytesWrote)/duration.Seconds())))
}

func writeBytes(chunks chan Chunk, chunkReturn chan Chunk, returnBytes chan FileBytes, conn net.Conn) {
	for chunk := range chunks {
		for _, shard := range *chunk.shards {
			conn.Write(shard)
		}

		returnBytes <- *chunk.fileBytes
		chunkReturn <- chunk
	}
	conn.Write(make([]byte, 1))
	close(returnBytes)
	close(chunkReturn)
}

func sliced(shards [][]byte) [][]byte {
	result := make([][]byte, len(shards))
	for i, shard := range shards {
		result[i] = shard
	}
	return result
}

func createEncoder(config utils.FREDConfig) reedsolomon.Encoder {
	enc, err := reedsolomon.New(config.DataShards, config.ParityShards)
	utils.CheckError(err, true)

	return enc
}

func createConnection(config utils.FREDConfig) net.Conn {
	raddr, err := net.ResolveUDPAddr("udp4", config.Raddr)
	utils.CheckError(err, true)
	conn, err := net.DialUDP("udp4", nil, raddr)
	utils.CheckError(err, true)

	return conn
}

func loadBytes(config utils.FREDConfig) (fileBytes chan FileBytes, returnBytes chan FileBytes) {
	fileBytes = make(chan FileBytes, config.ChannelDepth)
	returnBytes = make(chan FileBytes, cap(fileBytes))
	chunkSize := config.PacketSize * config.DataShards
	for x := 0; x < cap(fileBytes); x++ {
		returnBytes <- FileBytes{0, make([]byte, chunkSize)}
	}

	go func(fileBytes chan FileBytes) {
		switch config.Benchmark {
		case true:
			loadBenchmarkBytes(fileBytes, returnBytes, config)
		case false:
			loadFileBytes(fileBytes, returnBytes, config)
		}
	}(fileBytes)

	return fileBytes, returnBytes
}

func loadBenchmarkBytes(fileBytes chan FileBytes, returnBytes chan FileBytes, config utils.FREDConfig) {
	for x := config.Size; x > 0; {
		chunk := <-returnBytes
		chunk.Size = max(0, min(x, cap(chunk.Bytes)))
		x -= chunk.Size
		fileBytes <- chunk
	}

	close(fileBytes)
}

func loadFileBytes(fileBytes chan FileBytes, returnBytes chan FileBytes, config utils.FREDConfig) {
	file, err := os.Open(config.Args[0])
	utils.CheckError(err, true)
	reader := bufio.NewReader(file)
	// 	reader := bufio.NewReaderSize(file, config.FBufferSize)

	for x := config.Size; x > 0; {
		chunk := <-returnBytes
		chunk.Size, err = reader.Read(chunk.Bytes)
		x -= chunk.Size
		fileBytes <- chunk
	}

	close(fileBytes)
}
