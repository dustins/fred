package io

import (
	"net"
	"time"
)

type FConn struct {
	conn net.Conn
}

func Dial(network, address string) (net.Conn, error) {
	conn, err := net.Dial(network, address)
	return &FConn{conn}, err
}

func (f *FConn) Read(buf []byte) (int, error) {
	return f.conn.Read(buf)
}

func (f *FConn) Write(buf []byte) (int, error) {
	return f.conn.Write(buf)
}

func (f *FConn) LocalAddr() net.Addr {
	return f.conn.LocalAddr()
}

func (f *FConn) RemoteAddr() net.Addr {
	return f.conn.RemoteAddr()
}

func (f *FConn) Close() error {
	return f.conn.Close()
}

func (f *FConn) SetDeadline(t time.Time) error {
	return f.conn.SetDeadline(t)
}

func (f *FConn) SetReadDeadline(t time.Time) error {
	return f.conn.SetReadDeadline(t)
}

func (f *FConn) SetWriteDeadline(t time.Time) error {
	return f.conn.SetWriteDeadline(t)
}
