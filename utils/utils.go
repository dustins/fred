package utils

import (
	"fmt"
	"log"
	"os"
)

type FREDConfig struct {
	Args         []string
	Raddr        string
	PacketSize   int
	DataShards   int
	ParityShards int
	Benchmark    bool
	Size         int
	FBufferSize  int
	ChannelDepth int
	Delay        int
}

func CheckError(err error, shouldExit bool) (result bool) {
	if err != nil {
		if shouldExit {
			log.Fatalf("Error: %s\n", err)
			os.Exit(1)
		}
	}

	return false
}

func ByteCountIEC(b int64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.2f %ciB",
		float64(b)/float64(div), "KMGTPE"[exp])
}
